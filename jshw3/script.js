const n = prompt("Введіть число:");
if (isNaN(n) || n === "") {
  alert("Ви ввели неправильне значення");
} else {
  let numbers = [];
  for (let i = 0; i <= n; i++) {
    if (i % 5 === 0) {
      numbers.push(i);
    }
  }
  if (numbers.length === 0) {
    console.log("Sorry, no numbers");
  } else {
    console.log(`Числа кратні 5: ${numbers.join(", ")}`);
  }
}